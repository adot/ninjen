use std::collections::BTreeMap;
use std::fmt::Display;
use std::io::Write;
use std::os::unix::ffi::OsStringExt;
use std::path::PathBuf;

#[derive(Debug, PartialEq, Default)]
pub struct Writer {
    inner: Vec<u8>,
}

pub trait IntoPaths: Sized {
    fn into_paths(self) -> Vec<u8>;
}

impl IntoPaths for Vec<u8> {
    fn into_paths(self) -> Vec<u8> {
        self
    }
}

impl IntoPaths for String {
    fn into_paths(self) -> Vec<u8> {
        self.into_bytes()
    }
}

impl IntoPaths for PathBuf {
    fn into_paths(self) -> Vec<u8> {
        // TODO: Windows suport
        self.into_os_string().into_vec()
    }
}

impl<T: IntoPaths> IntoPaths for Vec<T> {
    fn into_paths(self) -> Vec<u8> {
        let mut ret = Vec::new();
        for i in self.into_iter().map(IntoPaths::into_paths) {
            if !ret.is_empty() {
                ret.push(b' ');
            }
            ret.extend_from_slice(&i);
        }
        ret
    }
}

impl<T> IntoPaths for &T
where
    T: ToOwned + ?Sized,
    T::Owned: IntoPaths,
{
    fn into_paths(self) -> Vec<u8> {
        self.to_owned().into_paths()
    }
}
// TODO: Const impl when 1.51 comes out.

impl Writer {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn newline(&mut self) {
        self.inner.push(b'\n')
    }

    pub fn variable(&mut self, key: &str, value: &str) {
        self.vari(key, value, 0);
    }

    pub fn pool(&mut self, name: &str, depth: u32) {
        writeln!(self.inner, "pool {}", name).unwrap();
        self.vari("depth", depth, 1)
    }

    pub fn include(&mut self, path: &str) {
        writeln!(self.inner, "include {}", path).unwrap();
    }

    pub fn done(self) -> Vec<u8> {
        self.inner
    }

    pub fn subninja(&mut self, path: &str) {
        writeln!(self.inner, "subninja {}", path).unwrap();
    }

    pub fn default(&mut self, paths: &[&str]) {
        writeln!(self.inner, "default {}", paths.join(" ")).unwrap()
    }

    pub fn rule(&mut self, r: impl Into<Rule>) {
        macro_rules! write_if {
            ($name:expr, $val: expr) => {
                if let Some(s) = $val {
                    self.vari($name, s, 1)
                }
            };
        }

        let r: Rule = r.into();
        writeln!(self.inner, "rule {}", r.name).unwrap();
        self.vari("command", r.command, 1);

        write_if!("description", r.description);
        write_if!("depfile", r.depfile);
        if r.generator {
            self.vari("generator", "1", 1);
        }
        write_if!("pool", r.pool);
        if r.restat {
            self.vari("restat", "1", 1);
        }
        write_if!("rspfile", r.rspfile);
        write_if!("rspfile_content", r.rspfile_content);
        write_if!("deps", r.deps);
    }

    pub fn build<A, B, C>(&mut self, b: Build<A, B, C>)
    where
        A: IntoPaths,
        B: IntoPaths,
        C: IntoPaths,
    {
        // TODO: Do this
        // assert!(!b.outputs.is_empty());
        assert!(!b.rule.is_empty());
        let outs = b.outputs.into_paths();
        let ins = b.inputs.into_paths();
        let implicits = b.implicit.into_paths();

        // writeln!(self.inner, "build {}: {} {}", outs, b.rule, ins).unwrap();

        self.inner.extend_from_slice(b"build ");
        self.inner.extend_from_slice(&outs);
        self.inner.extend_from_slice(b": ");
        self.inner.extend_from_slice(b.rule.as_bytes());
        self.inner.push(b' ');
        self.inner.extend_from_slice(&ins);
        if !implicits.is_empty() {
            self.inner.extend_from_slice(b" | ");
            self.inner.extend_from_slice(&implicits);
        }
        // TODO: Windows??
        self.inner.push(b'\n');

        if let Some(p) = b.pool {
            self.vari("pool", p, 1);
        }
        for (k, v) in b.variables {
            self.vari(k, v, 1);
        }
    }

    fn vari(&mut self, key: impl Display, value: impl Display, indent: usize) {
        writeln!(self.inner, "{}{} = {}", "  ".repeat(indent), key, value).unwrap();
    }
}

#[derive(Debug, PartialEq, Default)]
//#[non_exhaustive]
pub struct Rule {
    pub name: String,
    pub command: String,
    pub description: Option<String>,
    pub depfile: Option<String>,
    pub generator: bool,
    pub pool: Option<String>,
    pub restat: bool,
    pub rspfile: Option<String>,
    pub rspfile_content: Option<String>,
    pub deps: Option<String>,
}

#[derive(Debug, PartialEq, Default)]
//#[non_exhaustive]
pub struct Build<A, B, C>
where
    A: IntoPaths,
    B: IntoPaths,
    C: IntoPaths,
{
    pub outputs: A,
    pub rule: &'static str,
    pub inputs: B,
    pub implicit: C,
    pub variables: BTreeMap<&'static str, String>,
    pub pool: Option<String>,
}

impl<X, Y> From<(X, Y)> for Rule
where
    X: Into<String>,
    Y: Into<String>,
{
    fn from((name, command): (X, Y)) -> Rule {
        let name = name.into();
        let command = command.into();
        Rule {
            name,
            command,
            ..Default::default()
        }
    }
}

impl<X, Y, Z> From<(X, Y, Z)> for Rule
where
    X: Into<String>,
    Y: Into<String>,
    Z: Into<String>,
{
    fn from((name, command, decription): (X, Y, Z)) -> Rule {
        let name = name.into();
        let command = command.into();
        let description = Some(decription.into());
        Rule {
            name,
            command,
            description,
            ..Default::default()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use literally::bmap;
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    fn test(c: impl Fn(&mut Writer)) {
        let mut n = Writer::new();
        c(&mut n);
        let str = String::from_utf8(n.done()).unwrap();
        insta::assert_snapshot!(str);
    }

    #[test]
    fn basic() {
        test(|n| {
            n.variable("key", "value");
            n.newline();
            n.pool("pool", 5);
            n.include("foo.ninja");
            n.subninja("bax.ninja");
        })
    }

    #[test]
    fn build() {
        test(|n| {
            // TODO: Find a way to remove the need for this type inference
            n.build::<_, _, &str>(Build {
                outputs: "out",
                rule: "cc",
                inputs: "in",
                variables: bmap! {
                    "name" => "value".to_owned()
                },
                ..Default::default()
            })
        })
    }

    #[test]
    fn implicit() {
        test(|n| {
            n.build(Build {
                outputs: "o",
                rule: "cc",
                inputs: "i",
                implicit: "io",
                ..Default::default()
            })
        })
    }

    mod into_paths {
        use std::path::Path;

        use super::*;

        #[test]
        fn simple() {
            fn t(x: impl IntoPaths) {
                assert_eq!(x.into_paths(), b"hello")
            }

            let x: &str = "hello";
            let z: String = "hello".to_owned();
            let y: &[u8] = b"hello";
            let w: Vec<u8> = "hello".to_owned().into_bytes();
            let h = PathBuf::from("hello");
            let i = Path::new("hello");

            t(w);
            t(x);
            t(y);
            t(z);
            t(h);
            t(i);
        }

        #[test]
        fn multi() {
            fn t(x: impl IntoPaths) {
                assert_eq!(String::from_utf8(x.into_paths()).unwrap(), "one two three");
            }

            t("one two three");
            let x: &[_] = &["one", "two", "three"];
            t(x);
            // TODO: This doesnt work as type inference gets &[&str; 3], not &[&str]
            // This is fixed by min_const_generics
            // t(&["one", "two", "three"]);
            t(vec!["one", "two", "three"]);
            t(vec!["one".to_owned(), "two".to_owned(), "three".to_owned()])
        }
    }
}
